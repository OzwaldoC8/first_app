json.array!(@comentarios) do |comentario|
  json.extract! comentario, :id, :post_id.integer, :body.text
  json.url comentario_url(comentario, format: :json)
end
